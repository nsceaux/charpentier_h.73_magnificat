\header {
  copyrightYear = "2011"
  composer = "Marc-Antoine Charpentier"
  opus = "H.73"
  title = \markup \center-column {
    \line { Prelude sur une basse obligee }
    \line { pour Magnificat a 3 voix }
    \line { sur la mesme basse avec simph[onie] }
  }
  editions = \markup { Mélanges autographes, volume 15, feuillets 42 à 46. }
}

%% LilyPond options:
#(ly:set-option 'ancient-style (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'ancient-alteration (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'baroque-repeats (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'baroque-repeat-bar ":||:")

%% Staff size:
#(set-global-staff-size
  (cond ((memq (ly:get-option 'part) '(voix)) 16) ;; vocal part
        ((ly:get-option 'part) 18)      ;; other parts
        ((eqv? #t (ly:get-option 'ancient-style)) 12) ;; make urtext smaller
        (else 14)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}


\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"
\setPath "."

\header {
  maintainer = \markup {
    Nicolas Sceaux, Sébastien Amadieu –
    \with-url #"http://www.precipitations.com" \smallCaps Précipitations
  }
}

%%% Titling
\paper {
  bookTitleMarkup = \markup \when-property #'header:title \abs-fontsize #12 \column {
    \column {
      \fontsize #1 \bold \fill-line { \fromproperty #'header:title }
      \null
      \fontsize #0 \fill-line {
        \fromproperty #'header:opus
        \fromproperty #'header:instrument
        \fromproperty #'header:composer
      }
      \vspace #1
    }
  }
}

pieceTocTitle =
#(define-music-function (parser location title) (markup?)
  (let ((rehearsal (rehearsal-number))
        (font-size (if (symbol? (ly:get-option 'part)) 2 3)))
    (add-toc-item parser 'tocPieceMarkup title rehearsal)
    (add-toplevel-markup parser 
      (if (eqv? #t (ly:get-option 'use-rehearsal-numbers))
          (markup #:rehearsal-number rehearsal
                  #:hspace 1
                  #:fontsize font-size title)
          (markup #:fill-line (#:fontsize font-size title))))
    (add-no-page-break parser)
    (make-music 'Music 'void #t)))

%%%

\opusPartSpecs
#`((dessus "Dessus" ()
           (;;
            #:score "score-dessus"
            #:notes "dessus"
            #:clef "treble"))
   (basse "Basse continue" ()
          (;;
           #:notes "basse" #:clef "basse"
           #:score-template "score-basse-continue-voix")))


%% ton original ou tranposition en mi mineur avec l'option `transpo'
global = 
#(define-music-function (parser this-location) ()
   (with-location #f
  (let* ((global-symbol
          (string->symbol (format #f "global~a~a" (*path*) (*piece*))))
         (global-music (ly:parser-lookup global-symbol)))
   (if (not (ly:music? global-music))
       (let* ((global-file (include-pathname "global")))
         (set! global-music
               (if (eqv? #t (ly:get-option 'transpo))
                   #{ \notemode { \transpose re mi { \include $global-file } } #}
                   #{ \notemode { \include $global-file } #}))
         (ly:parser-define! global-symbol global-music)))
   (ly:music-deep-copy global-music))))

includeNotes = 
#(define-music-function (parser this-location pathname) (string?)
   (with-location #f
     (let ((include-file (include-pathname pathname)))
       (if (eqv? #t (ly:get-option 'transpo))
           #{ \notemode { \transpose sol mi { \include $include-file } } #}
           #{ \notemode { \include $include-file } #}))))
